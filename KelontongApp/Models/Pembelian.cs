//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KelontongApp.Models
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    public partial class Pembelian
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Pembelian()
        {
            this.DetailPembelians = new HashSet<DetailPembelian>();
        }
    
        public string ID_Pembelian { get; set; }
        public string ID_Suplier { get; set; }
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string Keterangan { get; set; }
        public Nullable<decimal> Total { get; set; }
        private DBEntities db = new DBEntities();

        public Suplier ModelSuplier
        {
            get
            {
                return db.Supliers.Where(x => x.ID_Suplier == ID_Suplier).SingleOrDefault();
            }
            set
            {
                ID_Suplier = value.ID_Suplier;
            }
        }

        public ICollection ListBeli
        {
            get
            {
                return db.DetailPembelians.Where(x => x.ID_Pembelian == ID_Pembelian).ToList();
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DetailPembelian> DetailPembelians { get; set; }
        public virtual Suplier Suplier { get; set; }
    }
}
