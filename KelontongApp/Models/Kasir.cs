//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KelontongApp.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Kasir
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Kasir()
        {
            this.Penjualans = new HashSet<Penjualan>();
        }
    
        public string ID_Kasir { get; set; }
        public string Nama { get; set; }
        public string Alamat { get; set; }
        public Nullable<System.DateTime> Tgl_Lahir { get; set; }
        public string Notelp { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Penjualan> Penjualans { get; set; }
    }
}
