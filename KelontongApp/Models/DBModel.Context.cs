﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KelontongApp.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class DBEntities : DbContext
    {
        public DBEntities()
            : base("name=DBEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<DetailPembelian> DetailPembelians { get; set; }
        public virtual DbSet<DetailPenjualan> DetailPenjualans { get; set; }
        public virtual DbSet<Kasir> Kasirs { get; set; }
        public virtual DbSet<Kategori> Kategoris { get; set; }
        public virtual DbSet<Pelanggan> Pelanggans { get; set; }
        public virtual DbSet<Pembelian> Pembelians { get; set; }
        public virtual DbSet<Penjualan> Penjualans { get; set; }
        public virtual DbSet<Produk> Produks { get; set; }
        public virtual DbSet<Suplier> Supliers { get; set; }
    }
}
