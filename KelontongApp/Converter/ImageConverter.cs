﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using static System.Net.Mime.MediaTypeNames;
using System.Windows.Media.Imaging;
using KelontongApp.ViewModels;

namespace KelontongApp.Converter
{
    public class ImageConverter : BaseViewModel
    {
        public BitmapImage ConvertToBitmap(string fileName)
        {
            using (Stream bmpStream = System.IO.File.Open(fileName, System.IO.FileMode.Open))
            {
                BitmapImage image = new BitmapImage();
                image.BeginInit();
                image.CacheOption = BitmapCacheOption.OnLoad; // here
                image.StreamSource = bmpStream;
                image.EndInit();
                return image;
            }
        }

        public byte[] BitmapImage_To_ByteArray(BitmapImage _bitmapImage)
        {
            byte[] bytes;
            //
            using (MemoryStream ms = new MemoryStream())
            {
                JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(_bitmapImage));
                encoder.Save(ms);
                bytes = ms.ToArray();
            }
            //
            return bytes;
        }

        public BitmapImage ToImage(byte[] array)
        {
            using (var ms = new System.IO.MemoryStream(array))
            {
                var image = new BitmapImage();
                image.BeginInit();
                image.CacheOption = BitmapCacheOption.OnLoad; // here
                image.StreamSource = ms;
                image.EndInit();
                return image;
            }
        }
    }
}
