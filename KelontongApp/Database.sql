--? Membuat Table Kategori

CREATE TABLE Kategori
(
    ID_Kategori char(5) primary key ,
    Nama_Kategori varchar(40),
);


--? Membuat Table Produk

CREATE TABLE Produk
(
    ID_Produk char(5) primary key ,
    ID_Kategori char(5) FOREIGN KEY REFERENCES Kategori(ID_Kategori),
    Nama VARCHAR(40),
    HargaBeli MONEY ,
    HargaJual MONEY,
    Stok SMALLINT,
    Image IMAGE, 
);

--? Membuat Table Pelanggan

CREATE TABLE Pelanggan
(
    ID_Pelanggan char(5) primary key ,
    Nama varchar(40) ,
    Alamat varchar(70),
    Umur tinyint,
    Notelp varchar (12)
);

--? Membuat Table Kasir

CREATE TABLE Kasir
(
    ID_Kasir char(5) primary key ,
    Nama varchar(40) ,
    Alamat varchar(70),
    Tgl_Lahir date,
    Notelp varchar (12)
);

--? Membuat Table Suplier

CREATE TABLE Suplier
(
    ID_Suplier char(5) primary key ,
    Nama varchar(40),
    Alamat varchar(70),
    Notelp varchar(15),
);

--? Membuat Table Penjualan

CREATE TABLE Penjualan
(
    ID_Penjualan char(5) primary key ,
    ID_Pelanggan char(5) FOREIGN KEY REFERENCES Pelanggan (ID_Pelanggan),
    ID_Kasir char(5) FOREIGN KEY REFERENCES Kasir (ID_Kasir),
    Tanggal date,
    Total MONEY
);

--? Membuat Table DetailJual

CREATE TABLE DetailPenjualan
(
    ID_DetailJual int IDENTITY(1,1) primary key  ,
    ID_Produk char(5) FOREIGN KEY REFERENCES Produk(ID_Produk),
    ID_Penjualan char(5) FOREIGN KEY REFERENCES Penjualan (ID_Penjualan),
    Jumlah SMALLINT,
    Subtotal MONEY
);

--? Membuat Table Pembelian

CREATE TABLE Pembelian
(
    ID_Pembelian char(5) primary key ,
    ID_Suplier char(5) FOREIGN KEY REFERENCES Suplier(ID_Suplier ),
    Tanggal date,
    Keterangan varchar(70),
    Total MONEY
);

--? Membuat Table DetailPembelian

CREATE TABLE DetailPembelian
(
    ID_DetailBeli int IDENTITY(1,1) primary key ,
    ID_Pembelian char(5) FOREIGN KEY REFERENCES Pembelian(ID_Pembelian),
    ID_Produk char(5) FOREIGN KEY REFERENCES Produk(ID_Produk ),
    Jumlah SMALLINT,
    Subtotal MONEY
);

