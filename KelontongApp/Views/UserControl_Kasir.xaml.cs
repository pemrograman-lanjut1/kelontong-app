﻿using KelontongApp.ViewModels;
using KelontongApp.Views.PopUp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KelontongApp.Views
{
    /// <summary>
    /// Interaction logic for UserControl_Kasir.xaml
    /// </summary>
    public partial class UserControl_Kasir : UserControl
    {
        public UserControl_Kasir()
        {
            InitializeComponent();
            DataContext = vm;
        }
        KasirViewModel vm = new KasirViewModel();

        private async void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            await Task.Delay(1);
            NoDetail.Visibility = Visibility.Visible;
            DetailProduk.Visibility = Visibility.Hidden;
        }

        ConfirmDelete confirmDelete;
        ConfirmUpdate confirmUpdate;
        private async void BtnUpdate_Click(object sender, RoutedEventArgs e)
        {
            await Task.Delay(1);
            confirmUpdate = new ConfirmUpdate(vm);
            confirmUpdate.ShowDialog();
        }

        private async void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            await Task.Delay(1);
            confirmDelete = new ConfirmDelete(vm);
            confirmDelete.ShowDialog();
        }

        private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            NoDetail.Visibility = Visibility.Hidden;
            DetailProduk.Visibility = Visibility.Visible;
        }
    }
}
