﻿using KelontongApp.ViewModels;
using KelontongApp.Views.PopUp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KelontongApp.Views
{
    /// <summary>
    /// Interaction logic for UserControl_Penjualan.xaml
    /// </summary>
    public partial class UserControl_Penjualan : UserControl
    {
        public UserControl_Penjualan()
        {
            InitializeComponent();
            vm = new PenjualanViewModel();
            DataContext = vm;
        }
        private DetailJual popup;
        private PenjualanViewModel vm;
        private async void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            await Task.Delay(0);
            if (vm.ModelPenjualan != null)
            {
                popup = new DetailJual(vm);
                popup.ShowDialog();
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            vm.ModelPenjualan = new Models.Penjualan();
            vm.ListJual.Clear();
            vm.TempTotalJual = 0;
        }
    }
}
