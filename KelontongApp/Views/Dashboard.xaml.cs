﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KelontongApp.Views
{
    /// <summary>
    /// Interaction logic for PanelMenu.xaml
    /// </summary>
    public partial class Dashboard : Window
    {
        public Dashboard()
        {
            InitializeComponent();
            ChangeColor();
        }

        private int Index = 0;
        private void ProdukClick(object sender, MouseButtonEventArgs e)
        {
            Index = 0;
            ChangeColor();
            App.ViewRouting(false);
            App.ViewRouting(true, new UserControl_Produk());
        }

        private void PembelianClick(object sender, MouseButtonEventArgs e)
        {
            Index = 1;
            ChangeColor();
            App.ViewRouting(false);
            App.ViewRouting(true, new UserControl_Pembelian());
        }

        private void PenjualanClick(object sender, MouseButtonEventArgs e)
        {
            Index = 2;
            ChangeColor();
            App.ViewRouting(false);
            App.ViewRouting(true, new UserControl_Penjualan());
        }

        private void KasirClick(object sender, MouseButtonEventArgs e)
        {
            Index = 3;
            ChangeColor();
            App.ViewRouting(false);
            App.ViewRouting(true, new UserControl_Kasir());
        }

        private void SuplierClick(object sender, MouseButtonEventArgs e)
        {
            Index = 4;
            ChangeColor();
            App.ViewRouting(false);
            App.ViewRouting(true, new UserControl_Suplier());
        }

        private void PelangganClick(object sender, MouseButtonEventArgs e)
        {
            Index = 5;
            ChangeColor();
            App.ViewRouting(false);
            App.ViewRouting(true, new UserControl_Pelanggan());

        }

        private void AboutClick(object sender, MouseButtonEventArgs e)
        {
            Index = 6;
            ChangeColor();
            App.ViewRouting(false);
            App.ViewRouting(true, new UserControl_About());
        }

        private void ChangeColor()
        {
            var noclick = (SolidColorBrush)(new BrushConverter().ConvertFrom("#303A456C"));
            var click = (SolidColorBrush)(new BrushConverter().ConvertFrom("#336CFB"));
            MenuProduk.Foreground = noclick;
            MenuPembelian.Foreground = noclick;
            MenuPenjualan.Foreground = noclick;
            MenuKasir.Foreground = noclick;
            MenuSuplier.Foreground = noclick;
            MenuPelanggan.Foreground = noclick;
            MenuAbout.Foreground = noclick;
            switch (Index)
            {
                case 0:
                    MenuProduk.Foreground = click;
                    break;
                case 1:
                    MenuPembelian.Foreground = click;
                    break;
                case 2:
                    MenuPenjualan.Foreground = click;
                    break;
                case 3:
                    MenuKasir.Foreground = click;
                    break;
                case 4:
                    MenuSuplier.Foreground = click;
                    break;
                case 5:
                    MenuPelanggan.Foreground = click;
                    break;
                case 6:
                    MenuAbout.Foreground = click;
                    break;
            }
        }
    }
}
