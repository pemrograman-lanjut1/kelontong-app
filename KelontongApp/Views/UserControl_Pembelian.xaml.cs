﻿using KelontongApp.ViewModels;
using KelontongApp.Views.PopUp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KelontongApp.Views
{
    /// <summary>
    /// Interaction logic for UserControl_Pembelian.xaml
    /// </summary>
    public partial class UserControl_Pembelian : UserControl
    {
        public UserControl_Pembelian()
        {
            InitializeComponent();
            vm = new PembelianViewModel();
            DataContext = vm;
        }
        private PembelianViewModel vm;
        private DetailBeli popup;
        private async void LstPembelian_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            await Task.Delay(0);
            if (vm.ModelPembelian != null)
            {
                popup = new DetailBeli(vm);
                popup.ShowDialog();
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            vm.ModelPembelian = new Models.Pembelian();
            vm.ListBeli.Clear();
            vm.TempTotalBeli = 0;
        }
    }
}
