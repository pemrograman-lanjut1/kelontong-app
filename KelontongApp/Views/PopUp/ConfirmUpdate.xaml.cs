﻿using KelontongApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KelontongApp.Views.PopUp
{
    /// <summary>
    /// Interaction logic for ConfirmUpdate.xaml
    /// </summary>
    public partial class ConfirmUpdate : Window
    {
        public ConfirmUpdate(KasirViewModel kasir = null, PelangganViewModel pelanggan = null, ProdukViewModel produk = null, SuplierViewModel suplier = null)
        {
            InitializeComponent();
            if (kasir != null)
            {
                DataContext = kasir;
            }
            if (pelanggan != null)
            {
                DataContext = pelanggan;
            }
            if (produk != null)
            {
                DataContext = produk;
            }
            if (suplier != null)
            {
                DataContext = suplier;
            }
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
