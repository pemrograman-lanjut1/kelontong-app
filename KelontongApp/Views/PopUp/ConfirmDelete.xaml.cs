﻿using KelontongApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KelontongApp.Views.PopUp
{
    /// <summary>
    /// Interaction logic for ConfirmDelete.xaml
    /// </summary>
    public partial class ConfirmDelete : Window
    {
        public ConfirmDelete(KasirViewModel kasir=null, PelangganViewModel pelanggan= null, ProdukViewModel produk= null, SuplierViewModel suplier=null)
        {
            InitializeComponent();
            if (kasir != null)
            {
                _kasir = true;
                DataContext = kasir;
            }
            if (pelanggan != null)
            {
                _pelanggan = true;
                DataContext = pelanggan;
            }
            if (produk != null)
            {
                _produk = true;
                DataContext = produk;
            }
            if (suplier != null)
            {
                _suplier = true;
                DataContext = suplier;
            }

        }

        private Boolean _kasir = false;
        private Boolean _pelanggan = false;
        private Boolean _produk = false;
        private Boolean _suplier = false;
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
            await Task.Delay(1);
            if (_kasir)
            {
                App.ViewRouting(false);
                App.ViewRouting(true, new UserControl_Kasir());
            }
            if (_pelanggan)
            {
                App.ViewRouting(false);
                App.ViewRouting(true, new UserControl_Pelanggan());
            }
            if (_suplier)
            {
                App.ViewRouting(false);
                App.ViewRouting(true, new UserControl_Suplier());
            }
            if (_produk)
            {
                App.ViewRouting(false);
                App.ViewRouting(true, new UserControl_Produk());
            }

            Close();
        }
    }
}
