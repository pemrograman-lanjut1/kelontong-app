﻿using System;
using System.Text;
using System.ComponentModel;
using System.Windows.Data;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Windows.Input;
using KelontongApp.Models;
using System.Windows;
using System.Linq;

namespace KelontongApp.ViewModels
{
    public class SuplierViewModel : BaseViewModel
    {
        public SuplierViewModel()
        {
            _listSuplier = new ObservableCollection<Suplier>();
            _modelSuplier = new Suplier();

            SuplierCollectionView = CollectionViewSource.GetDefaultView(ListSuplier);
            SuplierCollectionView.Filter = FilterSuplier;
            CreateCommand = new Command(async () => await CreateSuplier());
            UpdateCommand = new Command(async () => await UpdateSuplier());
            DeleteCommand = new Command(async () => await DeleteSuplier());
            ReadCommand = new Command(async () => await ReadSuplier());
            ReadCommand.Execute(null);
        }

        //* Command
        public ICommand ReadCommand { get; set; }
        public ICommand CreateCommand { get; set; }
        public ICommand UpdateCommand { get; set; }
        public ICommand DeleteCommand { get; set; }

        //* CollectionView
        public ICollectionView SuplierCollectionView { get; set; }

        //*Suplier
        private Suplier _modelSuplier;
        public Suplier ModelSuplier
        {
            get
            {
                return _modelSuplier;
            }
            set
            {
                SetProperty(ref _modelSuplier, value);
            }
        }

        //* List Suplier
        private ObservableCollection<Suplier> _listSuplier;
        public ObservableCollection<Suplier> ListSuplier
        {
            get
            {
                return _listSuplier;
            }
            set
            {
                SetProperty(ref _listSuplier, value);
            }
        }

        //* Selected List Suplier
        private Suplier _selectedSuplier;
        public Suplier SelectedSuplier
        {
            get
            {

                return _selectedSuplier;
            }
            set
            {
                SetProperty(ref _selectedSuplier, value);
            }
        }

        //* Filtering
        private string _suplierFilter = string.Empty;
        public string SuplierFilter
        {
            get
            {
                return _suplierFilter;
            }
            set
            {
                _suplierFilter = value;
                OnPropertyChanged(nameof(SuplierFilter));
                SuplierCollectionView.Refresh();
            }
        }
        private bool FilterSuplier(object obj)
        {

            if (obj is Suplier suplier)
            {
                return suplier.Nama.ToLower().Contains(SuplierFilter.ToLower()) || suplier.ID_Suplier.ToLower().Contains(SuplierFilter.ToLower()) || suplier.Alamat.ToLower().Contains(SuplierFilter.ToLower());
            }
            return false;
        }

        //*CRUD Database
        private DBEntities db = new DBEntities();

        //*Create
        private async Task CreateSuplier()
        {
            try
            {
                db.Supliers.Add(ModelSuplier);
                db.SaveChanges();
            }
            catch
            {
                MessageBox.Show("Gagal Create Data Suplier\nCek Lagi Data Anda");
            }
            finally
            {
                ModelSuplier = new Suplier();
                await ReadSuplier();
            }
        }

        //*Read
        private async Task ReadSuplier()
        {
            try
            {
                ListSuplier?.Clear();
                foreach (Suplier Suplier in db.Supliers)
                {
                    ListSuplier.Add(Suplier);
                }
            }
            catch
            {
                MessageBox.Show("Gagal Load Database Suplier\nPastikan Anda Menginstall Service SQL Server Terbaru ");
            }
            finally
            {
                SuplierCollectionView.Refresh();
            }
            await Task.Delay(0);
        }

        //*Update
        private async Task UpdateSuplier()
        {
            try
            {
                var result = db.Supliers.Where(x => x.ID_Suplier == SelectedSuplier.ID_Suplier).SingleOrDefault();
                if (result == null)
                {
                    MessageBox.Show("ID Tidak Ditemukan");
                }
                else
                {
                    MessageBox.Show(result.Nama);
                    result.ID_Suplier = SelectedSuplier.ID_Suplier;
                    result.Nama = SelectedSuplier.Nama;
                    result.Notelp = SelectedSuplier.Notelp;
                    result.Alamat = SelectedSuplier.Alamat;
                    db.SaveChanges();
                }

            }
            catch
            {
                MessageBox.Show("Gagal Update Database Suplier");
            }
            finally
            {
                await ReadSuplier();
            }
        }

        //*Delete
        private async Task DeleteSuplier()
        {
            try
            {
                db.Supliers.Attach(SelectedSuplier);
                db.Supliers.Remove(SelectedSuplier);
                db.SaveChanges();
            }
            catch
            {
                MessageBox.Show("DATA GAGAL DIHAPUS !!");
            }
            finally
            {
                await ReadSuplier();
            }
        }
    }

}
