﻿using System;
using System.Text;
using System.ComponentModel;
using System.Windows.Data;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Windows.Input;
using KelontongApp.Models;
using System.Windows;
using System.Linq;

namespace KelontongApp.ViewModels
{
    public class PembelianViewModel : BaseViewModel
    {
        public PembelianViewModel()
        {
            ListSuplier = new ObservableCollection<Suplier>(db.Supliers.ToList());
            ListProduk = new ObservableCollection<Produk>(db.Produks.ToList());

            _listBeli = new ObservableCollection<DetailPembelian>();
            _listPembelian = new ObservableCollection<Pembelian>();
            _modelDetailBeli = new DetailPembelian();
            _modelPembelian = new Pembelian();

            PembelianCollectionView = CollectionViewSource.GetDefaultView(ListPembelian);
            PembelianCollectionView.Filter = FilterPembelian;
            CreateCommand = new Command(async () => await CreatePembelian());
            AddItem = new Command(async () => await AddBeli());
            ReadCommand = new Command(async () => await ReadPembelian());
            ReadCommand.Execute(null);
        }

        public ICommand ReadCommand { get; set; }
        public ICommand AddItem { get; set; }
        public ICommand CreateCommand { get; set; }

        public ICollectionView PembelianCollectionView { get; set; }


        //Suplier
        public ObservableCollection<Suplier> ListSuplier
        {
            get; set;
        }

        //Produk
        public ObservableCollection<Produk> ListProduk
        {
            get; set;
        }

        // Filter
        private string _pembelianFilter = string.Empty;
        public string PembeliansFilter
        {
            get
            {
                return _pembelianFilter;
            }
            set
            {
                _pembelianFilter = value;
                OnPropertyChanged(nameof(PembeliansFilter));
                PembelianCollectionView.Refresh();
            }
        }
        private string SuplierFilter
        {
            get
            {
                return _pembelianFilter;
            }
            set
            {
                _pembelianFilter = value;
                OnPropertyChanged(nameof(SuplierFilter));
                PembelianCollectionView.Refresh();
            }
        }
        public Suplier PembelianSuplierFilter
        {
            set
            {
                SuplierFilter = value.ID_Suplier;
                OnPropertyChanged(nameof(PembelianSuplierFilter));
                PembelianCollectionView.Refresh();
            }
        }

        private bool FilterPembelian(object obj)
        {
            if (obj is Pembelian pembelian)
            {
                return pembelian.Tanggal.ToString().ToLower().Contains(PembeliansFilter.ToLower()) || pembelian.Keterangan.ToString().ToLower().Contains(PembeliansFilter.ToLower()) || pembelian.ID_Suplier.ToLower().Contains(SuplierFilter.ToLower());
            }
            return false;
        }

        private int _indexsort = 0;
        public int IndexSort
        {
            get
            {
                return _indexsort;
            }
            set
            {
                _indexsort = value;
                OnPropertyChanged(nameof(IndexSort));
                SortingPembelianList(_indexsort);
            }
        }
        private void SortingPembelianList(int a)
        {
            if (a == 1)
            {
                PembelianCollectionView.SortDescriptions.Clear();
                PembelianCollectionView.SortDescriptions.Add(new SortDescription(nameof(Pembelian.Total), ListSortDirection.Ascending));
            }
            else if (a == 0)
            {
                PembelianCollectionView.SortDescriptions.Clear();
                PembelianCollectionView.SortDescriptions.Add(new SortDescription(nameof(Pembelian.Total), ListSortDirection.Descending));
            }
            else
                PembelianCollectionView.SortDescriptions.Clear();
        }



        //*Pembelian
        private Pembelian _modelPembelian;
        public Pembelian ModelPembelian
        {
            get
            {
                return _modelPembelian;
            }
            set
            {
                SetProperty(ref _modelPembelian, value);
            }
        }

        private Pembelian _selectedPembelian;
        public Pembelian SelectedPembelian
        {
            get
            {
                return _selectedPembelian;
            }
            set
            {
                SetProperty(ref _selectedPembelian, value);
            }
        }

        //* List Pembelian
        private ObservableCollection<Pembelian> _listPembelian;
        public ObservableCollection<Pembelian> ListPembelian
        {
            get
            {
                return _listPembelian;
            }
            set
            {
                SetProperty(ref _listPembelian, value);
            }
        }
        public event Action OnReload;


        //* Item dibeli
        private decimal tempTotalBeli = 0;
        public decimal TempTotalBeli
        {
            get { return tempTotalBeli; }
            set { SetProperty(ref tempTotalBeli, value); }
        }
        private DetailPembelian _modelDetailBeli;
        public DetailPembelian ModelDetailBeli
        {
            get
            {
                return _modelDetailBeli;
            }
            set
            {
                SetProperty(ref _modelDetailBeli, value);
            }
        }

        private ObservableCollection<DetailPembelian> _listBeli;
        public ObservableCollection<DetailPembelian> ListBeli
        {
            get
            {
                return _listBeli;
            }
            set
            {
                SetProperty(ref _listBeli, value);
            }
        }

        private async Task AddBeli()
        {
            TempTotalBeli += Subtotal();
            ModelDetailBeli.Subtotal = Subtotal();
            ModelDetailBeli.ID_Pembelian = ModelPembelian.ID_Pembelian;
            ListBeli.Add(ModelDetailBeli);
            ModelDetailBeli = new DetailPembelian();
            OnReload?.Invoke();
            await Task.Delay(0);
        }

        decimal Subtotal()
        {
            return (int)ModelDetailBeli.Jumlah * (int)ModelDetailBeli.ModelProduk.HargaBeli;
        }
        // CRUD
         private DBEntities db = new DBEntities();
        //*Create
        private async Task CreatePembelian()
        {
            try
            {
                ModelPembelian.Total = TempTotalBeli;
                db.Pembelians.Add(ModelPembelian);
                db.SaveChanges();
                foreach(DetailPembelian item in ListBeli)
                {
                    var produk = db.Produks.Where(x => x.ID_Produk == item.ID_Produk).SingleOrDefault();
                    produk.Stok += item.Jumlah;
                    db.DetailPembelians.Add(item);
                }

                db.SaveChanges();
            }
            catch
            {
                MessageBox.Show("Gagal Create Data Pembelian\nCek Lagi Data Anda");
            }
            finally
            {
                ListBeli = new ObservableCollection<DetailPembelian>();
                ModelPembelian = new Pembelian();
                TempTotalBeli = 0;
                await ReadPembelian();
            }
        }

        //*Read
        private async Task ReadPembelian()
        {
            try
            {
                ListPembelian?.Clear();
                foreach (Pembelian Pembelian in db.Pembelians)
                {
                    ListPembelian.Add(Pembelian);
                }
            }
            catch
            {
                MessageBox.Show("Gagal Load Database Pembelian\nPastikan Anda Menginstall Service SQL Server Terbaru ");
            }
            finally
            {
                PembelianCollectionView.Refresh();
            }
            await Task.Delay(0);
        }
    }
}
