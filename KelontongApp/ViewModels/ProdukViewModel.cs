﻿using System;
using System.Text;
using System.ComponentModel;
using System.Windows.Data;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Windows.Input;
using KelontongApp.Models;
using System.Linq;
using System.Windows;
using Microsoft.Win32;

namespace KelontongApp.ViewModels
{
    public class ProdukViewModel : BaseViewModel
    {
        public ProdukViewModel()
        {
            _listKategori = new ObservableCollection<Kategori>();
            _listProduk = new ObservableCollection<Produk>();
            _modelProduk = new Produk();
            _selectedKategori = new Kategori();

            ListKategori = new ObservableCollection<Kategori> (db.Kategoris.ToList()) ;
            ProdukCollectionView = CollectionViewSource.GetDefaultView(ListProduk);
            ProdukCollectionView.Filter = FilterProduk;

            LoadfileCommand = new Command(async () => await LoadImage());
            CreateCommand = new Command(async () => await CreateProduk());
            UpdateCommand = new Command(async () => await UpdateProduk());
            DeleteCommand = new Command(async () => await DeleteProduk());
            ReadCommand = new Command(async () => await ReadProduk());
            ReadCommand.Execute(null);

        }

        //? Command
        public ICommand ReadCommand { get; set; }
        public ICommand CreateCommand { get; set; }
        public ICommand UpdateCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        public ICommand LoadfileCommand { get; set; }

        //? CollectionView
        public ICollectionView ProdukCollectionView { get; set; }


        //Produk
        private Produk _modelProduk;
        public Produk ModelProduk
        {
            get
            {
                return _modelProduk;
            }
            set
            {
                SetProperty(ref _modelProduk, value);
            }
        }

        private ObservableCollection<Produk> _listProduk;
        public ObservableCollection<Produk> ListProduk
        {
            get
            {
                return _listProduk;
            }
            set
            {
                SetProperty(ref _listProduk, value);
            }
        }

        private Produk _selectedProduk;
        public Produk SelectedProduk
        {
            get
            {
                
                return _selectedProduk;
            }
            set
            {
                SetProperty(ref _selectedProduk, value);
            }
        }


        // Kategori
        private ObservableCollection<Kategori> _listKategori;
        public ObservableCollection<Kategori> ListKategori
        {
            get { return _listKategori; }
            set { SetProperty(ref _listKategori, value); }
        }

        private Kategori _selectedKategori;
        public Kategori SelectedKategori
        {
            get {
                return _selectedKategori; 
            }
            set { SetProperty(ref _selectedKategori, value); }
        }

        // Foto Produk
        private async Task LoadImage()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Pilih Image Produk";
            openFileDialog.Filter = ".jpg .jpeg .png |*.jpg;*.jpeg;*.png";
            bool? respone = openFileDialog.ShowDialog();

            if (respone == true)
            {
                string filepath = openFileDialog.FileName;
                ModelProduk.Filename = filepath;
            }
            await Task.Delay(0);
        }

        // Filtering & Sorting
        private string _produkFilter = string.Empty;
        public string ProduksFilter
        {
            get
            {
                return _produkFilter;
            }
            set
            {
                _produkFilter = value;
                OnPropertyChanged(nameof(ProduksFilter));
                ProdukCollectionView.Refresh();
            }
        }
        private string KategoriFilter
        {
            get
            {
                return _produkFilter;
            }
            set
            {
                _produkFilter = value;
                OnPropertyChanged(nameof(KategoriFilter));
                ProdukCollectionView.Refresh();
            }
        }
        public Kategori ProdukKategoriFilter
        {
            set
            {
                KategoriFilter = value.ID_Kategori;
                OnPropertyChanged(nameof(ProdukKategoriFilter));
                ProdukCollectionView.Refresh();
            }
        }

        private bool FilterProduk(object obj)
        {
            if (obj is Produk produk)
            {
                return produk.Nama.ToLower().Contains(ProduksFilter.ToLower()) || produk.ID_Kategori.ToLower().Contains(KategoriFilter.ToLower());
            }
            return false;
        }

        private int _indexsort = 0;
        public int IndexSort
        {
            get
            {
                return _indexsort;
            }
            set
            {
                _indexsort = value;
                OnPropertyChanged(nameof(IndexSort));
                SortingProdukList(_indexsort);
            }
        }
        private void SortingProdukList(int a)
        {
            if (a == 1)
            {
                ProdukCollectionView.SortDescriptions.Clear();
                ProdukCollectionView.SortDescriptions.Add(new SortDescription(nameof(Produk.Stok), ListSortDirection.Ascending));
            }
            else if (a == 0)
            {
                ProdukCollectionView.SortDescriptions.Clear();
                ProdukCollectionView.SortDescriptions.Add(new SortDescription(nameof(Produk.Stok), ListSortDirection.Descending));
            }
            else
                ProdukCollectionView.SortDescriptions.Clear();

        }


        // CRUD Database
        private DBEntities db = new DBEntities();
        // Create
        private async Task CreateProduk ()
        {
            ModelProduk.ID_Kategori = SelectedKategori.ID_Kategori;
            try
            {
                db.Produks.Add(ModelProduk);
                db.SaveChanges();
            }
            catch
            {
                MessageBox.Show("Gagal Create Data Produk\nCek Lagi Data Anda");
            }
            finally
            {
                ModelProduk = new Produk();
                await ReadProduk();
            }
        }

        // Read
        private async Task ReadProduk()
        {
            try
            {
                ListProduk?.Clear();
                foreach(Produk produk in db.Produks)
                {
                    ListProduk.Add(produk);
                }
            }
            catch
            {
                MessageBox.Show("Gagal Load Database Produk\nPastikan Anda Menginstall Service SQL Server Terbaru ");
            }
            finally 
            {
                ProdukCollectionView.Refresh();
            }
            await Task.Delay(0);
        }

        // Update
        private async Task UpdateProduk()
        {
            try
            {
                var result = db.Produks.Where(x => x.ID_Produk == SelectedProduk.ID_Produk).SingleOrDefault();
                if (result == null)
                {
                    MessageBox.Show("ID Tidak boleh di ubah !!");
                }
                else
                {
                    MessageBox.Show(result.Nama);
                    result.ID_Produk = SelectedProduk.ID_Produk;
                    result.Nama = SelectedProduk.Nama;
                    result.HargaBeli = SelectedProduk.HargaBeli;
                    result.HargaJual = SelectedProduk.HargaJual;
                    result.Stok = SelectedProduk.Stok;
                    db.SaveChanges();
                }

            }
            catch
            {
                MessageBox.Show("Gagal Update Database Produk");
            }
            finally
            {
                await ReadProduk();
            }
        }

        //Delete
        private async Task DeleteProduk()
        {
            try
            {
                db.Produks.Attach(SelectedProduk);
                db.Produks.Remove(SelectedProduk);
                db.SaveChanges();
            }
            catch
            {
                MessageBox.Show("DATA GAGAL DIHAPUS !!\nCek Kembali Data Anda ");
            }
            finally
            {
                await ReadProduk();
            }
        }
    }


}
