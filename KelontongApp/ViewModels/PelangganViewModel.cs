﻿using System;
using System.Text;
using System.ComponentModel;
using System.Windows.Data;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Windows.Input;
using KelontongApp.Models;
using System.Windows;
using System.Linq;

namespace KelontongApp.ViewModels
{
    public class PelangganViewModel : BaseViewModel
    {
        public PelangganViewModel()
        {
            _listPelanggan = new ObservableCollection<Pelanggan>();
            _modelPelanggan = new Pelanggan();

            PelangganCollectionView = CollectionViewSource.GetDefaultView(ListPelanggan);
            PelangganCollectionView.Filter = FilterPelanggan;
            CreateCommand = new Command(async () => await CreatePelanggan());
            UpdateCommand = new Command(async () => await UpdatePelanggan());
            DeleteCommand = new Command(async () => await DeletePelanggan());
            ReadCommand = new Command(async () => await ReadPelanggan());
            ReadCommand.Execute(null);
        }

        //* Command
        public ICommand ReadCommand { get; set; }
        public ICommand CreateCommand { get; set; }
        public ICommand UpdateCommand { get; set; }
        public ICommand DeleteCommand { get; set; }

        //* CollectionView
        public ICollectionView PelangganCollectionView { get; set; }

        //*Pelanggan
        private Pelanggan _modelPelanggan;
        public Pelanggan ModelPelanggan
        {
            get
            {
                return _modelPelanggan;
            }
            set
            {
                SetProperty(ref _modelPelanggan, value);
            }
        }

        //* List Pelanggan
        private ObservableCollection<Pelanggan> _listPelanggan;
        public ObservableCollection<Pelanggan> ListPelanggan
        {
            get
            {
                return _listPelanggan;
            }
            set
            {
                SetProperty(ref _listPelanggan, value);
            }
        }

        //* Selected List Pelanggan
        private Pelanggan _selectedPelanggan;
        public Pelanggan SelectedPelanggan
        {
            get
            {

                return _selectedPelanggan;
            }
            set
            {
                SetProperty(ref _selectedPelanggan, value);
            }
        }

        //* Filtering
        private string _pelangganFilter = string.Empty;
        public string PelangganFilter
        {
            get
            {
                return _pelangganFilter;
            }
            set
            {
                _pelangganFilter = value;
                OnPropertyChanged(nameof(PelangganFilter));
                PelangganCollectionView.Refresh();
            }
        }
        private bool FilterPelanggan(object obj)
        {

            if (obj is Pelanggan pelanggan)
            {
                return pelanggan.Nama.ToLower().Contains(PelangganFilter.ToLower()) || pelanggan.ID_Pelanggan.ToLower().Contains(PelangganFilter.ToLower()) || pelanggan.Alamat.ToLower().Contains(PelangganFilter.ToLower());
            }
            return false;
        }

        //*CRUD Database
        private DBEntities db = new DBEntities();

        //*Create
        private async Task CreatePelanggan()
        {
            try
            {
                db.Pelanggans.Add(ModelPelanggan);
                db.SaveChanges();
            }
            catch
            {
                MessageBox.Show("Gagal Create Data Pelanggan\nCek Lagi Data Anda");
            }
            finally
            {
                ModelPelanggan = new Pelanggan();
                await ReadPelanggan();
            }
        }

        //*Read
        private async Task ReadPelanggan()
        {
            try
            {
                ListPelanggan?.Clear();
                foreach (Pelanggan Pelanggan in db.Pelanggans)
                {
                    ListPelanggan.Add(Pelanggan);
                }
            }
            catch
            {
                MessageBox.Show("Gagal Load Database Pelanggan\nPastikan Anda Menginstall Service SQL Server Terbaru ");
            }
            finally
            {
                PelangganCollectionView.Refresh();
            }
            await Task.Delay(0);
        }

        //*Update
        private async Task UpdatePelanggan()
        {
            try
            {
                var result = db.Pelanggans.Where(x => x.ID_Pelanggan == SelectedPelanggan.ID_Pelanggan).SingleOrDefault();
                if (result == null)
                {
                    MessageBox.Show("ID Tidak Ditemukan");
                }
                else
                {
                    MessageBox.Show(result.Nama);
                    result.ID_Pelanggan = SelectedPelanggan.ID_Pelanggan;
                    result.Nama = SelectedPelanggan.Nama;
                    result.Umur = SelectedPelanggan.Umur;
                    result.Notelp = SelectedPelanggan.Notelp;
                    result.Alamat = SelectedPelanggan.Alamat;
                    db.SaveChanges();
                }

            }
            catch
            {
                MessageBox.Show("Gagal Update Database Pelanggan");
            }
            finally
            {
                await ReadPelanggan();
            }
        }

        //*Delete
        private async Task DeletePelanggan()
        {
            try
            {
                db.Pelanggans.Attach(SelectedPelanggan);
                db.Pelanggans.Remove(SelectedPelanggan);
                db.SaveChanges();
            }
            catch
            {
                MessageBox.Show("DATA GAGAL DIHAPUS !!");
            }
            finally
            {
                await ReadPelanggan();
            }
        }
    }

}
