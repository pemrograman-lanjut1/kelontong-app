﻿using System;
using System.Text;
using System.ComponentModel;
using System.Windows.Data;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Windows.Input;
using KelontongApp.Models;
using System.Windows;
using System.Linq;

namespace KelontongApp.ViewModels
{
    public class PenjualanViewModel : BaseViewModel
    {
        public PenjualanViewModel()
        {
            ListKasir = new ObservableCollection<Kasir>(db.Kasirs.ToList());
            ListProduk = new ObservableCollection<Produk>(db.Produks.ToList());
            ListPelanggan = new ObservableCollection<Pelanggan>(db.Pelanggans.ToList());

            _listJual = new ObservableCollection<DetailPenjualan>();
            _listPenjualan = new ObservableCollection<Penjualan>();
            _modelDetailJual = new DetailPenjualan();
            _modelPenjualan = new Penjualan();

            PenjualanCollectionView = CollectionViewSource.GetDefaultView(ListPenjualan);
            PenjualanCollectionView.Filter = FilterPenjualan;
            CreateCommand = new Command(async () => await CreatePenjualan());
            AddItem = new Command(async () => await AddJual());
            ReadCommand = new Command(async () => await ReadPenjualan());
            ReadCommand.Execute(null);
        }

        public ICommand ReadCommand { get; set; }
        public ICommand AddItem { get; set; }
        public ICommand CreateCommand { get; set; }

        public ICollectionView PenjualanCollectionView { get; set; }
        //Kasir
        public ObservableCollection<Kasir> ListKasir
        {
            get; set;
        }
        public ObservableCollection<Pelanggan> ListPelanggan
        {
            get; set;
        }
        //Produk
        public ObservableCollection<Produk> ListProduk
        {
            get; set;
        }

        // Filter
        private string _penjualanFilter = string.Empty;
        public string PenjualansFilter
        {
            get
            {
                return _penjualanFilter;
            }
            set
            {
                _penjualanFilter = value;
                OnPropertyChanged(nameof(PenjualansFilter));
                PenjualanCollectionView.Refresh();
            }
        }
        private string KasirFilter
        {
            get
            {
                return _penjualanFilter;
            }
            set
            {
                _penjualanFilter = value;
                OnPropertyChanged(nameof(KasirFilter));
                PenjualanCollectionView.Refresh();
            }
        }
        public Kasir PenjualanKasirFilter
        {
            set
            {
                KasirFilter = value.ID_Kasir;
                OnPropertyChanged(nameof(PenjualanKasirFilter));
                PenjualanCollectionView.Refresh();
            }
        }

        private bool FilterPenjualan(object obj)
        {
            if (obj is Penjualan penjualan)
            {
                return penjualan.Tanggal.ToString().ToLower().Contains(PenjualansFilter.ToLower()) || penjualan.ID_Kasir.ToLower().Contains(KasirFilter.ToLower());
            }
            return false;
        }

        private int _indexsort = 0;
        public int IndexSort
        {
            get
            {
                return _indexsort;
            }
            set
            {
                _indexsort = value;
                OnPropertyChanged(nameof(IndexSort));
                SortingPenjualanList(_indexsort);
            }
        }
        private void SortingPenjualanList(int a)
        {
            if (a == 1)
            {
                PenjualanCollectionView.SortDescriptions.Clear();
                PenjualanCollectionView.SortDescriptions.Add(new SortDescription(nameof(Penjualan.Total), ListSortDirection.Ascending));
            }
            else if (a == 0)
            {
                PenjualanCollectionView.SortDescriptions.Clear();
                PenjualanCollectionView.SortDescriptions.Add(new SortDescription(nameof(Penjualan.Total), ListSortDirection.Descending));
            }
            else
                PenjualanCollectionView.SortDescriptions.Clear();
        }



        //*Penjualan
        private Penjualan _modelPenjualan;
        public Penjualan ModelPenjualan
        {
            get
            {
                return _modelPenjualan;
            }
            set
            {
                SetProperty(ref _modelPenjualan, value);
            }
        }

        private Penjualan _selectedPenjualan;
        public Penjualan SelectedPenjualan
        {
            get
            {
                return _selectedPenjualan;
            }
            set
            {
                SetProperty(ref _selectedPenjualan, value);
            }
        }

        //* List Penjualan
        private ObservableCollection<Penjualan> _listPenjualan;
        public ObservableCollection<Penjualan> ListPenjualan
        {
            get
            {
                return _listPenjualan;
            }
            set
            {
                SetProperty(ref _listPenjualan, value);
            }
        }
        public event Action OnReload;


        //* Item diJual
        private decimal tempTotalJual = 0;
        public decimal TempTotalJual
        {
            get { return tempTotalJual; }
            set { SetProperty(ref tempTotalJual, value); }
        }
        private DetailPenjualan _modelDetailJual;
        public DetailPenjualan ModelDetailJual
        {
            get
            {
                return _modelDetailJual;
            }
            set
            {
                SetProperty(ref _modelDetailJual, value);
            }
        }

        private ObservableCollection<DetailPenjualan> _listJual;
        public ObservableCollection<DetailPenjualan> ListJual
        {
            get
            {
                return _listJual;
            }
            set
            {
                SetProperty(ref _listJual, value);
            }
        }

        private async Task AddJual()
        {
            TempTotalJual += Subtotal();
            ModelDetailJual.Subtotal = Subtotal();
            ModelDetailJual.ID_Penjualan = ModelPenjualan.ID_Penjualan;
            ListJual.Add(ModelDetailJual);
            ModelDetailJual = new DetailPenjualan();
            OnReload?.Invoke();
            await Task.Delay(0);
        }

        decimal Subtotal()
        {
            return (int)ModelDetailJual.Jumlah * (int)ModelDetailJual.ModelProduk.HargaJual;
        }
        // CRUD
        private DBEntities db = new DBEntities();
        //*Create
        private async Task CreatePenjualan()
        {
            try
            {
                ModelPenjualan.Total = TempTotalJual;
                db.Penjualans.Add(ModelPenjualan);
                db.SaveChanges();
                foreach (DetailPenjualan item in ListJual)
                {
                    var produk = db.Produks.Where(x => x.ID_Produk == item.ID_Produk).SingleOrDefault();
                    produk.Stok -= item.Jumlah;
                    db.DetailPenjualans.Add(item);
                }
                db.SaveChanges();
            }
            catch
            {
                MessageBox.Show("Gagal Create Data Penjualan\nCek Lagi Data Anda");
            }
            finally
            {
                ListJual = new ObservableCollection<DetailPenjualan>();
                ModelPenjualan = new Penjualan();
                TempTotalJual = 0;
                await ReadPenjualan();
            }
        }

        //*Read
        private async Task ReadPenjualan()
        {
            try
            {
                ListPenjualan?.Clear();
                foreach (Penjualan Penjualan in db.Penjualans)
                {
                    ListPenjualan.Add(Penjualan);
                }
            }
            catch
            {
                MessageBox.Show("Gagal Load Database Penjualan\nPastikan Anda Menginstall Service SQL Server Terbaru ");
            }
            finally
            {
                PenjualanCollectionView.Refresh();
            }
            await Task.Delay(0);
        }
    }

}
