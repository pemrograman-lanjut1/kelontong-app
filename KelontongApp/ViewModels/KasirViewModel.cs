﻿using System;
using System.Text;
using System.ComponentModel;
using System.Windows.Data;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Windows.Input;
using KelontongApp.Models;
using System.Windows;
using System.Linq;

namespace KelontongApp.ViewModels
{
    public class KasirViewModel : BaseViewModel
    {
        public KasirViewModel()
        {
            _listKasir = new ObservableCollection<Kasir>();
            _modelKasir = new Kasir();

            KasirCollectionView = CollectionViewSource.GetDefaultView(ListKasir);
            KasirCollectionView.Filter = FilterKasir;
           CreateCommand = new Command(async () => await CreateKasir());
            UpdateCommand = new Command(async () => await UpdateKasir());
            DeleteCommand = new Command(async () => await DeleteKasir());
            ReadCommand = new Command(async () => await ReadKasir());
            ReadCommand.Execute(null);
        }

        //* Command
        public ICommand ReadCommand { get; set; }
        public ICommand CreateCommand { get; set; }
        public ICommand UpdateCommand { get; set; }
        public ICommand DeleteCommand { get; set; }

        //* CollectionView
        public ICollectionView KasirCollectionView { get; set; }

        //*Kasir
        private Kasir _modelKasir;
        public Kasir ModelKasir
        {
            get
            {
                return _modelKasir;
            }
            set
            {
                SetProperty(ref _modelKasir, value);
            }
        }

        //* List Kasir
        private ObservableCollection<Kasir> _listKasir;
        public ObservableCollection<Kasir> ListKasir
        {
            get
            {
                return _listKasir;
            }
            set
            {
                SetProperty(ref _listKasir, value);
            }
        }

        //* Selected List Kasir
        private Kasir _selectedKasir;
        public Kasir SelectedKasir
        {
            get
            {

                return _selectedKasir;
            }
            set
            {
                SetProperty(ref _selectedKasir, value);
            }
        }

        //* Filtering
        private string _kasirFilter = string.Empty;
        public string KasirFilter
        {
            get
            {
                return _kasirFilter;
            }
            set
            {
                _kasirFilter = value;
                OnPropertyChanged(nameof(KasirFilter));
                KasirCollectionView.Refresh();
            }
        }
        private bool FilterKasir(object obj)
        {

            if (obj is Kasir kasir)
            {
                return kasir.Nama.ToLower().Contains(KasirFilter.ToLower()) || kasir.ID_Kasir.ToLower().Contains(KasirFilter.ToLower()) || kasir.Alamat.ToLower().Contains(KasirFilter.ToLower());
            }
            return false;
        }

        //*CRUD Database
        private DBEntities db = new DBEntities();

        //*Create
        private async Task CreateKasir()
        {
            try
            {
                db.Kasirs.Add(ModelKasir);
                db.SaveChanges();
            }
            catch
            {
                MessageBox.Show("Gagal Create Data Kasir\nCek Lagi Data Anda");
            }
            finally
            {
                ModelKasir = new Kasir();
                await ReadKasir();
            }
        }

        //*Read
        private async Task ReadKasir()
        {
            try
            {
                ListKasir?.Clear();
                foreach (Kasir Kasir in db.Kasirs)
                {
                    ListKasir.Add(Kasir);
                }
            }
            catch
            {
                MessageBox.Show("Gagal Load Database Kasir\nPastikan Anda Menginstall Service SQL Server Terbaru ");
            }
            finally
            {
                KasirCollectionView.Refresh();
            }
            await Task.Delay(0);
        }

        //*Update
        private async Task UpdateKasir()
        {
            try
            {
                var result = db.Kasirs.Where(x => x.ID_Kasir == SelectedKasir.ID_Kasir).SingleOrDefault();
                if (result == null)
                {
                    MessageBox.Show("ID Tidak Ditemukan");
                }
                else
                {
                    MessageBox.Show(result.Nama);
                    result.ID_Kasir = SelectedKasir.ID_Kasir;
                    result.Nama = SelectedKasir.Nama;
                    result.Notelp = SelectedKasir.Notelp;
                    result.Tgl_Lahir = SelectedKasir.Tgl_Lahir;
                    result.Alamat = SelectedKasir.Alamat;
                    db.SaveChanges();
                }

            }
            catch
            {
                MessageBox.Show("Gagal Update Database Kasir");
            }
            finally
            {
                await ReadKasir();
            }
        }

        //*Delete
        private async Task DeleteKasir()
        {
            try
            {
                db.Kasirs.Attach(SelectedKasir);
                db.Kasirs.Remove(SelectedKasir);
                db.SaveChanges();
            }
            catch
            {
                MessageBox.Show("DATA GAGAL DIHAPUS !!");
            }
            finally
            {
                await ReadKasir();
            }
        }
    }
}
